package nl.cc

import com.fasterxml.jackson.annotation.JsonInclude.*
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder


@Configuration
class Configuration {

    @Bean
    fun objectMapperBuilder() = Jackson2ObjectMapperBuilder
            .json()
            .serializationInclusion(Include.NON_NULL)
            .simpleDateFormat("yyyy-MM-dd")
            .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

}