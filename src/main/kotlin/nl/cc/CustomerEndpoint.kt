package nl.cc

import org.springframework.web.bind.annotation.*

@RequestMapping("/customers")
@RestController
class CustomerEndpoint(private val repository: CustomerRepository) {

    @GetMapping
    fun getAllCustomers() = repository.getAll()

    @GetMapping("{id}")
    fun getCustomerById(@PathVariable("id") id: Long) = repository.getById(id)

    @PostMapping
    fun createCustomer(@RequestBody createRequest: CreateCustomerDTO) = repository.save(createRequest)

    @DeleteMapping("{id}")
    fun deleteCustomerById(@PathVariable("id") id: Long) = repository.removeById(id)

    @PutMapping
    fun updateCustomer(@RequestBody updateRequest: UpdateCustomerDTO) = repository.save(updateRequest)

}